# Intensidad de Turbulencia y Reportes Mensuales

Proyecto de desarrollo de códigos para el procesamiento y análisis de datos obtenidos con los anemómetros del Laboratorio de Investigación en Energía Eólica del Tecnológico de Costa Rica. 

## Reportes mensuales
La generación de la estadísticas para los reportes de la rapidez e intensidad de turbulencia.

## Rapidez de muestreo y sensibilidad a la intensidad de turbulencia
Se busca además hacer comparaciones en los muestreos a 1 minuto y 1 segundo, para observar si hay pérdidas en la sensibilidad de la intensidad de turbulencia.

## Desarrolladores:

> Allan Josué González Villalobos

> Harold Campos Jiménez

> Bryaton Chaves Cordero
